package main

import (
	"github.com/gin-gonic/gin"
	"go-search/internal/line"
	"go-search/internal/restaurants"
	"go-search/internal/test"
	"net/http"
)

type Router struct {
	Name     string
	Path     string
	Method   string
	Endpoint gin.HandlerFunc
}

func getRouter() []Router {
	return []Router{
		{
			"Ping",
			"/ping",
			"POST",
			func(c *gin.Context) {
				c.String(http.StatusOK, "pong")
			},
		},
		{"Finding Restaurant at Bangsue",
			"/find_food_bangsue",
			"POST",
			restaurants.FindNearBangsue,
		},
		{
			"Finding Restaurant By Keyword",
			"/find_food_keyword",
			"POST",
			restaurants.FindNearByKeyword,
		},
		{
			"Finding XYZ By Question",
			"/find_value",
			"POST",
			test.FindingValue,
		},
		{
			"Line call back",
			"/callback_line",
			"POST",
			line.Callback,
		},
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	router := getRouter()
	for _, listRouter := range router {
		r.Handle(listRouter.Method, listRouter.Path, listRouter.Endpoint)
	}
	return r
}

package main

import (
	cors "github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"go-search/configs"
	"google.golang.org/appengine"
	"time"
)

var db = make(map[string]string)

func main() {
	configs.ReadConfig();
	allows := []string{
		"*",
	}
	r := setupRouter()
	r.Use(CostomCors(allows))

	r.Run(":" + configs.AppConf.Port)
	appengine.Main()
}

// CostomCors returns the location middleware with default configuration.
func CostomCors(allows []string) gin.HandlerFunc {
	conf := cors.Config{
		AllowOrigins:     allows,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "OPTIONS"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type", "Accept", "Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}
	return cors.New(conf)
}

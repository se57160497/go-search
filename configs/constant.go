package configs

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
	"os"
)

type AppConfiguration struct {
	Environment string
	Port        string
	Google      *GoogleConf
}

type GoogleConf struct {
	UrlNearBySearch string `mapstructure:"url_near_by_search"`
	Key          string
}

var AppConf AppConfiguration

func ReadConfig() {
	fmt.Println("OS ENVIROMENT : ", os.Getenv("STAGE"))

	if os.Getenv("STAGE") == "PRODUCTION" {
		viper.SetConfigName("config")
	} else {
		viper.SetConfigName("config.dev")
	}

	viper.SetConfigType("json")
	viper.AutomaticEnv()
	viper.AddConfigPath("./configs")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	err := viper.Unmarshal(&AppConf)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	var test interface{}
	err = viper.Unmarshal(&test)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	fmt.Println("ENVIRONMENT : ", AppConf.Environment)
	fmt.Println("KEY : ", AppConf.Google)
	fmt.Println("test : ", test)
}

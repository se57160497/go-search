package restaurants

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"go-search/internal/google"
	"net/http"
)

func FindNearByKeyword(c *gin.Context) {
	var req RequestKeyword
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	parameter := fmt.Sprintf(`location=%f,%f&radius=%f&type=%s&keyword=%s`,
		req.Location.Latitude,
		req.Location.Longitude,
		req.Radius,
		req.Type,
		req.Keyword,
	)
	resFromGoogle := google.NearBySearch(parameter)
	var res google.ResponseGoogleStruct
	err := json.Unmarshal(resFromGoogle, &res);
	if err != nil {
		fmt.Println("err : ", err)
	}
	c.JSON(http.StatusOK, res)
	return
}

func FindNearBangsue(c *gin.Context) {
	resFromGoogle := google.NearBySearch("location=13.806289,100.537804&radius=1500&type=restaurant")
	fmt.Println(string(resFromGoogle));
	var res google.ResponseGoogleStruct
	err := json.Unmarshal(resFromGoogle, &res);
	if err != nil {
		fmt.Println("err : ", err)
	}
	c.JSON(http.StatusOK, res)
	return
}

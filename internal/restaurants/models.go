package restaurants

import "go-search/internal/google"

type RequestKeyword struct {
	Location *google.Location `json:"location" binding:"required"`
	Radius   float32           `json:"radius" binding:"required"`
	Type     string           `json:"type" binding:"required"`
	Keyword  string           `json:"keyword"`
}

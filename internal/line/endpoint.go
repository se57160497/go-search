package line

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/linebot"
	"go-search/configs"
	"go-search/internal/google"
	"log"
)

var bot *linebot.Client

func Callback(c *gin.Context) {
	//var req linebot.ViewReques
	bot, err := linebot.New("5d495aed72346c89cbd7d4f3dd22d518", "DJ8Azf4XX1cNBCLUGxMN1wAvFiFBWOBo0+UjSCryLG2GtxRPi+xGFd94YoB4GLf8hLemjvk/L5HUPvoQFYDjiGR78tuv+ntXZ1NuxH+3JFZFbaWsY8sTUB4NeND1W2l1JqAMyy83L0ixPZfF+jjDOQdB04t89/1O/w1cDnyilFU=")

	events, err := bot.ParseRequest(c.Request)
	if err != nil {
		if err == linebot.ErrInvalidSignature {
			c.AbortWithStatus(400)
		} else {
			c.AbortWithStatus(500)
		}
		return
	}

	for _, event := range events {
		if event.Type == linebot.EventTypeMessage {
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				_, err := bot.GetMessageQuota().Do()
				if err != nil {
					log.Println("Quota err:", err)
				}
				if message.Text == "!หิวจัง" {
					resFromGoogle := google.NearBySearch("location=13.806289,100.537804&radius=1500&type=restaurant")
					fmt.Println(string(resFromGoogle));
					var res google.ResponseGoogleStruct
					err := json.Unmarshal(resFromGoogle, &res);
					if err != nil {
						fmt.Println("err : ", err)
					}

					var columns []*linebot.CarouselColumn

					for index, value := range res.Result {
						// New TemplateAction
						if index < 10 {
							var actions []linebot.TemplateAction
							actions = append(actions, linebot.NewMessageAction("ขอที่อยู่", value.Vicinity))
							open := "ตอนนี้ปิด"
							if value.OpeningHours.OpenNow {
								open = "ตอนนี้เปิด"
							}
							des := fmt.Sprintf(`%s : %.2f ดาว (%d)`, open, value.Rating, value.UserRatingsTotal)
							img := "https://cdn.dribbble.com/users/1012566/screenshots/4187820/topic-2.jpg"
							if len(value.Photos) > 0 {
								img = fmt.Sprintf(`https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%s&key=%s`, value.Photos[0].PhotoReference, configs.AppConf.Google.Key)
							}
							columns = append(columns, linebot.NewCarouselColumn(img, value.Name, des, actions...))
						}
					}
					carousel := linebot.NewCarouselTemplate(columns...)
					// New TemplateMessage
					template := linebot.NewTemplateMessage("Carousel", carousel)
					if _, err = bot.ReplyMessage(event.ReplyToken, template).Do(); err != nil {
						log.Print(err)
					}
				} else {
					//if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(message.ID+":"+message.Text+" OK! remain message:"+strconv.FormatInt(quota.Value, 10))).Do(); err != nil {
					if _, err = bot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage(message.Text)).Do(); err != nil {
						log.Print(err)
					}
				}
			}
		}
	}

	return
}

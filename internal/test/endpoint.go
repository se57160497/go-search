package test

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RequestTest struct {
	Question []interface{} `json:"question" binding:"required"`
}

func FindingValue(c *gin.Context) {
	var req RequestTest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var List []float64
	var flag = false;
	var flagFirst = false;
	var FirstValue float64
	var indexFirstValue int
	if len(req.Question) > 0 {
		for i, value := range req.Question {
			switch v := value.(type) {
			case float64:
				List = append(List, v)
				flag = false;
				if !flagFirst {
					FirstValue = v
					indexFirstValue = i
					flagFirst = true;
				}
			case string:
				flag = true;
			default:
				flag = true;
			}
			if flag && len(List) > 1 {
				break;
			}
		}
		var Diff []float64
		var V float64
		for index, _ := range List {
			if index+1 < len(List) {
				value := List[index] - List[index+1];
				if len(Diff) > 0 {
					V = Diff[len(Diff)-1] - value
				} else {
					V = value
				}
				Diff = append(Diff, value)
			}
		}
		result := make(map[int]float64)
		for i := indexFirstValue; i >= 0; i-- {
			if i == indexFirstValue {
				fmt.Println(1)
				result[i] = FirstValue
			} else {
				fmt.Println(2)
				result[i] = result[i+1] - (V * float64(i+1))
			}
		}
		for i := indexFirstValue; i < len(req.Question); i++ {
			fmt.Println(i == indexFirstValue)
			if i == indexFirstValue {
				result[i] = FirstValue
			} else {
				result[i] = result[i-1] + (V * float64(i))
			}
		}
		c.JSON(http.StatusOK, result)
		return
	}
	c.JSON(http.StatusOK, List)
	return

}

package google

import (
	"go-search/configs"
	"io/ioutil"
	"net/http"
)

func NearBySearch(parameter string) []byte {
	googleKey := "key=" + configs.AppConf.Google.Key;
	if len(parameter) == 0 {
		parameter = googleKey
	} else {
		parameter += "&" + googleKey
	}
	var req *http.Request;
	req, _ = http.NewRequest("GET", configs.AppConf.Google.UrlNearBySearch+"?"+parameter, nil)
	req.Header.Add("cache-control", "no-cache")
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	return body
}

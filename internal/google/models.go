package google

type ResponseGoogleStruct struct {
	Result []Result `json:"results"`
}

type Result struct {
	Geometry         Geometry     `json:"geometry"`
	Icon             string        `json:"icon"`
	ID               string        `json:"id"`
	Name             string        `json:"name"`
	OpeningHours     OpeningHours `json:"opening_hours"`
	Photos           []Photo       `json:"photos"`
	PlaceID          string        `json:"place_id"`
	Reference        string        `json:"reference"`
	Scope            string        `json:"scope"`
	Vicinity         string        `json:"vicinity"`
	Rating           float32       `json:"rating"`
	UserRatingsTotal int64         `json:"user_ratings_total"`
	Types            []string      `json:"types"`
	PlusCode         PlusCode     `json:"plus_code"`
}

type PlusCode struct {
	CompoundCode string `json:"compound_code"`
	GlobalCode   string `json:"global_code"`
}

type Photo struct {
	Height           float32  `json:"height"`
	HtmlAttributions []string `json:"html_attributions"`
	PhotoReference   string   `json:"photo_reference"`
	Width            float32  `json:"width"`
}

type OpeningHours struct {
	OpenNow bool `json:"open_now"`
}

type Geometry struct {
	Location Location `json:"location"`
	ViewPort ViewPort `json:"viewport"`
}

type ViewPort struct {
	Northeast Location `json:"northeast"`
	Southwest Location `json:"southwest"`
}

type Location struct {
	Latitude  float64 `json:"lat"`
	Longitude float64 `json:"lng"`
}
